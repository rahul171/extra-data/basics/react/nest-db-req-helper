import React from 'react';
import axios from './services/axios';
import { output, helperClear } from './helper';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.setUpAxios();
  }

  setUpAxios() {
    axios.interceptors.response.use(data => {
      output(data);
    });
  }

  getUsers = () => {
    axios.get('/user');
  }

  getUsersRelations = () => {
    axios.get('/user/?relation=1');
  }

  getSubUsers = () => {
    axios.get('/subUser');
  }

  getSubUsersRelations = () => {
    axios.get('/subUser/?relation=1');
  }

  setData = () => {
    axios.post('/');
  }

  runThis = () => {
    axios.get('/run-this');
  }

  resetdb = () => {
    axios.get('/reset-db');
  }

  save = () => {
    axios.get('/save/2');
  }

  clear = () => {
    helperClear();
  }

  render() {
    return (
      <div>
        <div>
          <button onClick={this.getUsers}>users</button>
          <button onClick={this.getUsersRelations}>user R</button>
          <button onClick={this.getSubUsers}>subUsers</button>
          <button onClick={this.getSubUsersRelations}>subUsers R</button>
          <hr />
          <button onClick={this.setData}>set</button>
          <button onClick={this.save}>save</button>
          <hr />
          <button onClick={this.runThis}>runThis</button>
          <button onClick={this.resetdb}>reset db</button>
          <button onClick={this.clear}>clear</button>
        </div>
      </div>
    );
  }
}
