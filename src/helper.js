export function syntaxHighlight(json) {
  json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
  return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+-]?\d+)?)/g, function (match) {
    var cls = 'number';
    if (/^"/.test(match)) {
      if (/:$/.test(match)) {
        cls = 'key';
      } else {
        cls = 'string';
      }
    } else if (/true|false/.test(match)) {
      cls = 'boolean';
    } else if (/null/.test(match)) {
      cls = 'null';
    }
    return '<span class="' + cls + '">' + match + '</span>';
  });
}

export function getStr(obj) {
  let s = JSON.stringify(obj, function(k,v){
    if(v instanceof Array && typeof v[0] !== 'object')
      return JSON.stringify(v);
    return v;
  }, 2);
  // s=s.replace(/"(\w+)"\s*:/g, '$1:');
  return s;
}

export const output = (data) => {
  const outputElement = document.getElementById('output');
  outputElement.insertBefore(
    document.createElement('pre'),
    outputElement.firstChild
  ).innerHTML = syntaxHighlight(getStr(data));
}

export const helperClear = () => {
  const outputElement = document.getElementById('output');
  outputElement.innerHTML = '';
}
