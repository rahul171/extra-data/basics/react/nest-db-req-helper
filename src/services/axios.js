import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://localhost:3001/users'
});

instance.interceptors.response.use(response => {
  console.log('data =>', response.data);
  return response.data;
}, error => {
  console.log('error =>', error.response.data.error);
  return Promise.reject(error);
});

export default instance;
